# Future Mind Application

FM APP web application for task
- [Overall](#overall)
  - [Document scope and versioning](#document-scope-and-versioning)
  - [About project](#about-project)
  - [Run project](#run-project)
- [Notes](#notes)

# Overall
## Document scope and versioning

This document is an introduction for project developers. It briefly describes a project, introduces developer into a project.

| date       | version | change description | author                            |
|------------|---------|--------------------|-----------------------------------|
| 08.01.2023 | 0.0.1   | initial version    | Piotr Liwerski |

## About project

Application libs

| name       | version |
|------------|---------|
| ruamel.yaml.clib    | 0.2.7     |
| Pillow              | 9.4.0     |
| sqlparse            | 0.4.3     |
| MarkupSafe          | 2.1.1     |
| djangorestframework | 3.14.0    |
| gunicorn            | 20.1.0    |
| requests            | 2.28.1    |
| inflection          | 0.5.1     |
| uritemplate         | 4.1.1     |
| django-filter       | 22.1      |
| Jinja2              | 3.1.2     |
| drf-yasg            | 1.21.4    |
| asgiref             | 3.6.0     |
| charset-normalizer  | 2.1.1     |
| Django              | 3.2.16    |
| packaging           | 22.0      |
| psycopg2-binary     | 2.9.5     |
| coreschema          | 0.0.4     |
| python-dotenv       | 0.21.0    |
| pip                 | 22.0.4    |
| idna                | 3.4       |
| setuptools          | 65.6.3    |
| itypes              | 1.2.0     |
| urllib3             | 1.26.13   |
| pytz                | 2022.7    |
| ruamel.yaml         | 0.17.21   |
| certifi             | 2022.12.7 |
| coreapi             | 2.3.3     |


## Run project

Use a command ```make run``` \
NOTES:
1. App is hosted on 8000 port
2. During launch super user is created. Login: admin Password: admin123
3. Swagger is on ```/docs```
4. Admin panle is on ```/admin```
5. Used cURL to test
```
  curl -X POST \
  http://localhost:3000/v1/fm/ \
  -H 'cache-control: no-cache' \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -H 'postman-token: 18127c20-89cd-040f-7be0-a6a6749c30d0' \
  -F width=200 \
  -F height=200 \
  -F 'title=test title' \
  -F image=@test-image.jpg
```
6. After forget about removing one line I added test :)  To run it please be sure that containers are up and then execute ```make test``` \
My apologies


# Notes

This notes were specified by developer. Marking some key bullets of project.
1. Better was to used ```InMemoryFile``` and during request resize image not after save
2. In my opinin tests added here was not required due to use of standard libraries and simplicity of project. Only ```save()``` method has some logic.
3. I only added required endpoints, thats why I was not use ```ModelViewSet```
4. Storage strategy could be more precise. For example add relation with ```User``` to get username and create better structore if images files.