from django.test import TestCase

from images.models import FutureMindImages
class FutueMindImagesTestCase(TestCase):

    def test_save_object(self):
        with open("images/testFolder/test_image.png", "rb") as test_image:
            url = "/v1/fm/"
            self.client.post(url,{
                "title": "Test title",
                "width": 200,
                "height": 200,
                "image": test_image
            })

        self.assertEqual(FutureMindImages.objects.count(), 1)
        self.assertLessEqual(FutureMindImages.objects.first().image.height, 200)
        self.assertLessEqual(FutureMindImages.objects.first().image.width, 200)
