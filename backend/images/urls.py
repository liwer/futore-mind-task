from django.urls import include, path
from rest_framework.routers import DefaultRouter

from images.views import FutureMindImagesViewSet

router = DefaultRouter()
router.register("fm", FutureMindImagesViewSet, basename="fm")

urlpatterns = [
    path("", include(router.urls))
]
