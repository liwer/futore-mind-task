from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from images.models import FutureMindImages
from images.serializers import FutureMindImagesSerializer


class FutureMindImagesViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    GenericViewSet
    ):
    authentication_classes = []
    queryset = FutureMindImages.objects.all()
    serializer_class = FutureMindImagesSerializer
    filterset_fields = ("title",)
