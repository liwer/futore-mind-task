from django.db import models
from PIL import Image


class FutureMindImages(models.Model):
    title = models.CharField(max_length=32)
    width = models.IntegerField()
    height = models.IntegerField()
    image = models.ImageField(upload_to="images/")

    def __str__(self) -> str:
        return self.title

    def save(self, *args, **kwargs) -> None:
       super().save(*args, **kwargs)
       if self.image and self.width and self.height:
           img = Image.open(self.image.path)
           img.thumbnail((self.height, self.width))
           img.save(self.image.path)
