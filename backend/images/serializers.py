from rest_framework.serializers import ModelSerializer, ImageField

from images.models import FutureMindImages


class FutureMindImagesSerializer(ModelSerializer):
    image = ImageField(max_length=None, use_url=True)
    class Meta:
        model = FutureMindImages
        fields = ("image", "id", "height", "width", "title")
